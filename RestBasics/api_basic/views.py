from django.shortcuts import render
from django.http import HttpResponse,JsonResponse
from rest_framework.parsers import JSONParser
import os
from RestBasics.settings import BASE_DIR
from api_basic.models import Article
from api_basic.serializers import ArticleSerializer
from api_basic.cnn.cnn import get_model_predic
#to avoid error in the view you need to put csrf_exempt
from django.views.decorators.csrf import csrf_exempt

#with thia we do not nneed the @csrf_exempt used in the article_list and article_detail
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

#Using class base view
from rest_framework.views import APIView

#generic view
from rest_framework import generics
from rest_framework import mixins

#for authentification and generic view
from rest_framework.authentication import SessionAuthentication,BasicAuthentication,TokenAuthentication
from rest_framework.permissions import IsAuthenticated

#GenericAPIView
class GenericArticleAPIView(generics.GenericAPIView,mixins.ListModelMixin,mixins.CreateModelMixin,
                            mixins.UpdateModelMixin,mixins.RetrieveModelMixin,mixins.DestroyModelMixin):
    serializer_class = ArticleSerializer
    queryset = Article.objects.all()
    #for url pattern
    lookup_field = 'id'

    #with this authetification we use username and password basic auth
    #authentication_classes = [SessionAuthentication,BasicAuthentication]

    # with this authetification we use token authentification
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self,request,id = None):
        if id:
            return self.retrieve(request)
        else:
            return self.list(request)

    def post(self, request):
        return self.create(request)

    def put (self,request,id=None):
        return self.update(request,id)

    def delete(self,request,id):
        return  self.destroy(request,id)



#APIView
class ArticleAPIView(APIView):

    def get(self,request):
        print("aqui")
        articles = Article.objects.all()
        serializer = ArticleSerializer(articles, many=True )
        return Response(serializer.data)

    def post(self,request):
        serializer=ArticleSerializer(data=request.data)
        print(request.data)
        if serializer.is_valid():
            serializer_intance = serializer.save()

            print(serializer.data['article_pic'])
            url=serializer.data['article_pic']
            print(BASE_DIR)
            MEDIA_DATA = BASE_DIR + str(url)
            media =  MEDIA_DATA.replace('\\', '/')
            print(media)
            num_index = get_model_predic(media)
            flower_set = ['daisy','dandelion','rose','sunflower','tulips']
            serializer_intance.flower_type =flower_set[num_index]
            serializer_intance.save()
            flower_type =flower_set[num_index]

            return Response(data={"flower_type":flower_type},status=status.HTTP_201_CREATED)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
class ArticleDetailAPIView(APIView):

    def get_object(self,id):
        try:
            article = Article.objects.get(pk=id)
            return article
        except Article.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def get(self,request,id):
        article = self.get_object(id)
        serializer = ArticleSerializer(article)
        return Response(serializer.data)

    def put(self,request,id):
        article = self.get_object(id)
        serializer = ArticleSerializer(article, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        article = self.get_object(id)
        article.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#Function View
#to get one multiple and create one article function view
@api_view(['GET','POST'])
def article_list(request):
    print("aqui")
    #to see all article
    if request.method == 'GET':
        articles = Article.objects.all()
        serializer = ArticleSerializer(articles, many=True )
        return Response(serializer.data)

    #to ccreate a article
    elif request.method == 'POST':
        print("aqui")
        print(request.data)
        serializer=ArticleSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
#to get one single article function view
@api_view(['GET','PUT','DELETE'])
def article_detail(request,pk):

    try:
        article =  Article.objects.get(pk=pk)
    except Article.DoesNotExist:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    if request.method == 'GET':
        serializer = ArticleSerializer(article)

        return Response(serializer.data)
    elif request.method == 'PUT':

        serializer=ArticleSerializer(article,data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        article.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)