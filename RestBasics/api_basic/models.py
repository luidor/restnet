from django.db import models

# Create your models here.

class Article(models.Model):
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    flower_type = models.EmailField(max_length=100,blank=True)
    date = models.DateTimeField(auto_now_add=True)
    article_pic = models.ImageField(upload_to='profile_pics', blank=True)
    def __str__(self):
        return self.title


