from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense,Dropout
import keras
import keras.backend.tensorflow_backend as tb
tb._SYMBOLIC_SCOPE.value = True
import numpy as np
from keras.preprocessing import image
from PIL import Image
"""

# Initialising the CNN
model = Sequential()

# Step 1 - Convolution
model.add(Conv2D(128, (3, 3), input_shape=(224, 224, 3), activation='relu'))

# Step 2 - Pooling
model.add(MaxPooling2D(pool_size=(2, 2)))

# Adding a second convolutional layer
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

# Adding a second convolutional layer
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

# Adding a second convolutional layer
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

# Step 3 - Flattening
model.add(Flatten())

# Step 4 - Full connection
model.add(Dense(units=128, activation='relu'))
model.add(Dense(units=5, activation='softmax'))

model.summary()
model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['accuracy'])
json_config = model.to_json()
with open('model_config.json', 'w') as json_file:
    json_file.write(json_config)
"""


with open('static/model/model_config.json') as json_file:
    json_config = json_file.read()
model = keras.models.model_from_json(json_config)
model.load_weights('static/model/flower_cnn_2.h5')
model.summary()


def get_model_predic(image_url=''):
    img_pred = image.load_img(image_url,target_size=(224, 224))
    # img_pred.show()
    img_pred = image.img_to_array(img_pred)
    img_pred = np.expand_dims(img_pred, axis=0)
    rstl = model.predict(img_pred)

    value = np.argmax(rstl[0], axis=0)
    return  value


"""

            print(serializer.data['article_pic'])
            url=serializer.data['article_pic']
            print(BASE_DIR)
            MEDIA_DATA = BASE_DIR + str(url)
            media =  MEDIA_DATA.replace('\\', '/')
            print(media)
"""