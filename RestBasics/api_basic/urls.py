
from django.urls import path,include
from api_basic.views import article_list,article_detail,ArticleAPIView,ArticleDetailAPIView,GenericArticleAPIView


urlpatterns = [

    #path('article/', article_list),
    path('article/', ArticleAPIView.as_view()),

    #path('detail/<int:pk>/', article_detail),
    path('detail/<int:id>/', ArticleDetailAPIView.as_view()),

    path('generic/article/<int:id>/', GenericArticleAPIView.as_view()),


]